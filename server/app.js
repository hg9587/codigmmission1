// (function() {
//   'use strict';
// //fileBrowser ./server/app.js file
//   var express = require('express');
//   var path = require('path');
//   var logger = require('morgan');
//   var cookieParser = require('cookie-parser');
//   var bodyParser = require('body-parser');

//   var routes = require('./routes.js');

//   var app = express();

//   // view engine setup
//   app.set('views', path.join(__dirname, 'views'));
//   app.engine('html', require('ejs').renderFile);

//   //different
//   app.set('view engine', 'html');
//   //different

//   app.use(logger('dev'));
//   app.use(bodyParser.json());
//   app.use(bodyParser.urlencoded({
//     extended: true
//   }));
//   app.use(cookieParser());
  
  
// //different
//   app.use(express.static(path.join(__dirname, '../client')));
// //different

//   app.use('/', routes);

//   app.set('port', process.env.PORT || 3000);

//   var server = app.listen(app.get('port'), function() {
//     console.log('Express server listening on port ' + server.address().port);
//   });

//   module.exports = app;
// }());
