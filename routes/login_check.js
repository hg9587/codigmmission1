var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var router = express.Router();
var app = require('../app');//for mongoose database module sharing and hash function


var message = "";
var destination = "";
var userData;
var flag=1;
/* GET home page. */
router.post('/', function(req, res, next) {

	console.log("Login check page referenced");
	var request_id = req.body.id.toString();
	var request_pw = req.body.passwd.toString();
	userData = { id:request_id, passwd:app.hashPW(request_pw) };
	
	app.Account.find({id: request_id}, function(err, doc) {
		
		if(doc.length === 0){
			req.session.regenerate(function() {
				message = "해당 아이디가 없습니다";
				destination = "login";
				req.session.error = 'Authentication failed';	
				console.log("There is no that id");
				var reply ="<script>alert('" + message +"'); location.href='/" + destination + "';</script>";
				res.send(reply);
			});
		} else if(doc.length === 1) {
			if(doc[0].passwd === app.hashPW(request_pw)){
				req.session.regenerate(function() {
					message = "로그인 성공. 환영합니다 "+request_id +"님.";
					destination = "main";
					console.log("id,pw is matched.");
					req.session.user = userData;
					console.log(req.session.user);
					req.session.success = 'Authentication as ' + userData.id;
					console.log(req.session.success);
					console.log('Login Session Established as' + userData.id);
					console.log("session. regenerate function finished");
					var reply ="<script>alert('" + message +"'); location.href='/" + destination + "';</script>";
					res.cookie('nickname',userData.id,{ expires: new Date(Date.now() + 90000000), httpOnly:true});
					res.send(reply);
				});
				
			} else {
				req.session.regenerate(function() {	
					message = "패스워드가 틀립니다";
					destination = "login";
					req.session.error = 'Authentication failed';
					console.log("Wrong password");
					var reply ="<script>alert('" + message +"'); location.href='/" + destination + "';</script>";
					res.send(reply);
				});
			}
		} else {
			message ="정의되지 않은 에러입니다."
			destination = "";
			console.log("Undefined error");
			var reply ="<script>alert('" + message +"'); location.href='/" + destination + "';</script>";
			res.send(reply);
		}
		//var reply ="<script>alert('" + message +"'); location.href='/" + destination + "';</script>";
		//console.log(reply);
		//	res.send(reply);
	});
		
});




// var crypto = require('crypto');

// function hashPW(pwd) {
// 	return crypto.createHash('sha256').update(pwd).digest('base64').toString();
// };

// router.post('/', function(req, res, next) {

// 	// var request_id = req.body.id.toString();
// 	// var userData = userId.find({ id: request_id });
// 	console.log("Login check page referenced");
// 	var hi = app.hashPW('12321');
// 	var userData = { id:"einstrasse", passwd: hi };
// 	if(userData.passwd === hashPW(req.body.passwd.toString())) {
// 		req.session.regenerate(function() {
// 			req.session.user = userData;
// 			req.session.success = 'Authentication as ' + userData.id;
// 			res.redirect('/main');
// 			console.log('Login Success')
// 		});
// 	} else {
// 		req.session.regenerate(function() {
// 			var response = "<script>alert('Wrong Id or password'); location.href='/login';</script>";
// 			req.session.error = 'Authentication failed.';
// 			res.send(response);
// 			// res.redirect('/login');
// 			console.log('Login Failed');
// 		})
// 	}
	
// });



router.get('/', function(req, res, next) {
	var response = "invalid Access" +
		"<script>alert('invalid Access');" +
		"location.href='/'</script>";
	//res.render('login_check', { title: 'Login Check'} );
	res.send(response);
	
});

module.exports = router;




