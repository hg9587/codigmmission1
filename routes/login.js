var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var sess = "";
	var log = "login";
	if(req.session.user){
		sess = req.session.user.id.toString();
		sess += "님 환영합니다. ";
		log = "logout";
		var response = "<script> alert('you already have session');" +
			"location.href='/main'</script>";
		res.send(response);
	}
	else {
		res.render('login', { title: 'Login Page',
					  userId: sess,
							logger: log});
	}
});
module.exports = router;

