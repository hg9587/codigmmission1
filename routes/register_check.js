var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var app = require('../app');//for mongoose database module sharing

// function hashPW(pwd) {
// 	return crypto.createHash('sha256').update(pwd).digest('base64').toString();
// };

///Database Initialization////
/*

mongoose.connect('mongodb://localhost/codigm');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongodb connection error:'));
//에러 핸들러 설정
db.once('open', function() {
	console.log('mongodb connection established successfully');
	//디비 연결 성공 메시지 출력
});

var accountSchema = mongoose.Schema({
	id: String,
	passwd: String
});
accountSchema.methods.dump = function() {
	var message = "id: "+ this.id +", passwd: " + this.passwd;
	console.log(message);
}

var Account =  mongoose.model('account', accountSchema);

*/
///Database Initialization////

var message = "";
var destination = "";
/* GET home page. */
router.post('/', function(req, res, next) {

	console.log("Register check page referenced");
	var id_ = req.body.id.toString();
	var pw1 = req.body.passwd1.toString();
	var pw2 = req.body.passwd2.toString();
	console.log("id:"+id_+"\npw1:"+pw1+"\npw2:"+pw2);
	if(id_ === ""){
		message = "id를 입력해주세요";
		destination = "register";
		var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
		res.send(reply);
	}else if(pw1 === "") {
		message = "패스워드를 입력해주세요";
		destination = "register";
		var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
		res.send(reply);
		
	}else if(pw2 === "") {
		message = "패스워드를 입력해주세요";
		destination = "register";
		var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
		res.send(reply);
		
	}else{//모든 입력값이 채워졌을 경우
		app.Account.find({ id: id_}, function(err, docs) {
			if(docs.length > 0){
				console.log(id_+" is already registered id");
				message = "ID 중복입니다."
				destination = "register";
				var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
				res.send(reply);
			}else if(pw1 != pw2){
				message = "입력하신 두개의 패스워드가 서로 다릅니다."
				destination = "register";
				console.log("two password not matched error");
				var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
				res.send(reply);
			}else{
				var acc = new app.Account({ id: id_.toString(), passwd: app.hashPW(pw1.toString())});
				acc.save(function (err, instance) {
					if(err) return console.error(err);
					instance.dump();
					console.log("new account created successfully");
					message = "성공적으로 회원가입이 완료 되었습니다. 반갑습니다 "+id_+"님";
					destination = "login";
				var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
				res.send(reply);
				});
			}
		})
	}
	// var reply ="<script>alert('" + message.toString() +"'); location.href='/" + destination.toString() + "';</script>";
	// res.send(reply);
});


router.get('/', function(req, res, next) {
	var response = "invalid Access" +
		"<script>alert('invalid Access');" +
		"location.href='/'</script>";
	//res.render('register_check', { title: 'Register Check'} );
	res.send(response);
	console.log(app.local_v);
});
	
	

module.exports = router;
