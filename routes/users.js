var express = require('express');
var router = express.Router();
var app = require('../app');//for mongoose database module sharing


	var user_array = [];//declared as global variable

/* GET home page. */
router.get('/', function(req, res, next) {
	var sess = "";
	var log = "login";
	user_array = [];//initialization
	app.Account.find( function(err, docs) {
		if(err)
			return console.error(err);
		for(var i=0; i<docs.length; i++){	
			user_array.push(docs[i].id.toString());
		}
		if(req.session.user){
			sess = req.session.user.id.toString();
			sess += "님 환영합니다. ";
			log = "logout";
		}
  	res.render('users', { title: '유저 페이지',
					  	userId: sess,
					  	logger: log,
					  	user_arr: user_array });
  	//considering as asynchronous coding. insert all code in callback function
	});
});

module.exports = router;
