document.body.onkeydown = function( e ) {
    var keys = {
        // 37: 'left',
        // 39: 'right',
        // 40: 'down',
        // 38: 'rotate'
		74: 'left',//J key
        76: 'right',//L key
        75: 'down',//K key
        73: 'rotate'//I key
    };
    if ( typeof keys[ e.keyCode ] != 'undefined' && is_host) {
        keyPress( keys[ e.keyCode ] );
        render();
    }
};
